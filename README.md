# README

dockerとdocker-composeインストール

参考
https://qiita.com/taroshin/items/22bb360b18a0a24871dd


## 初回起動
ディレクトリ移動後

docker-compose build

docker-compose up

docker-compose run web rake db:create

## 起動後
http://localhost:3000/

## 作業終了時
docker-compose down

## 再開
docker-compose up

## 起動中コンテナ停止
docker ps -q | xargs docker stop

## コンテナ確認
docker ps

## コンテナに入る
docker exec -it NAMES bash

※NAMESはdocker psで入りたいコンテナのNAMEを確認して指定する